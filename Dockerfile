FROM maven:3.8.4-openjdk-17 as vertxbuilder
WORKDIR /build
COPY . /build
RUN mvn clean install


FROM openjdk:17-jdk-slim
ENV VERTICLE_HOME /usr/verticles
COPY --from=vertxbuilder /build/target/recruiting-*-fat.jar $VERTICLE_HOME/verticle.jar
WORKDIR $VERTICLE_HOME
RUN groupadd -r peasant \
    && useradd -r -s /bin/false -g peasant peasant \
    && chown -R peasant:peasant /usr/verticles
USER peasant
EXPOSE 9000
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar /usr/verticles/verticle.jar"]