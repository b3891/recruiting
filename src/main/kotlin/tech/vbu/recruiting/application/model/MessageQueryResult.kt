package tech.vbu.recruiting.application.model

import tech.vbu.recruiting.domain.Message

data class MessageQueryResult(val entries: Collection<Message>)
