package tech.vbu.recruiting.application

import tech.vbu.recruiting.application.model.MessageQueryResult


interface MessageLookupService {
    fun getMessages(): MessageQueryResult
}