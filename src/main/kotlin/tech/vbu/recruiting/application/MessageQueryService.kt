package tech.vbu.recruiting.application

import tech.vbu.recruiting.application.model.MessageQueryResult
import java.util.concurrent.CompletableFuture

class MessageQueryService(
    private val lookupService: MessageLookupService
) {
    fun getMessage(): CompletableFuture<MessageQueryResult> {
        val result = this.lookupService.getMessages()
        return CompletableFuture.completedFuture(result)
    }

}

