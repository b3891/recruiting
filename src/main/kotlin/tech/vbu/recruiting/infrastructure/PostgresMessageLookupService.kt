package tech.vbu.recruiting.infrastructure

import tech.vbu.recruiting.application.MessageLookupService
import tech.vbu.recruiting.application.model.MessageQueryResult
import tech.vbu.recruiting.domain.ContentType
import tech.vbu.recruiting.domain.Message
import javax.sql.ConnectionPoolDataSource

class PostgresMessageLookupService(private val dataSource: ConnectionPoolDataSource) : MessageLookupService {
    override fun getMessages(): MessageQueryResult {
        dataSource.pooledConnection.connection.use { conn ->
            conn.prepareStatement(SELECT_MESSAGES).use { stmt ->
                stmt.executeQuery().use { rs ->
                    val messages = mutableListOf<Message>()

                    while (rs.next()) {
                        messages.add(Message(rs.getString("data"), ContentType(rs.getString("content_type"))))
                    }

                    return MessageQueryResult(messages)
                }
            }
        }
    }
}

private const val SELECT_MESSAGES = "SELECT * FROM public.message"