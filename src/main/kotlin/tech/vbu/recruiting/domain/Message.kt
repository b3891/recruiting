package tech.vbu.recruiting.domain

data class Message(val data: String, val contentType: ContentType)

data class ContentType(val type: String)