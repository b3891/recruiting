package tech.vbu.recruiting.ui.http.message

import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import tech.vbu.recruiting.application.model.MessageQueryResult
import java.util.concurrent.CompletableFuture

fun getMessagesDataHandlerOf(
    getMessages: () -> CompletableFuture<MessageQueryResult>
) = { ctx: RoutingContext ->

    getMessages().thenApply { messageQueryResult ->
        ctx.response().end(Json.encode(messageQueryResult.entries))
    }
    Unit
}