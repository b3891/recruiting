package tech.vbu.recruiting.ui.http

import io.vertx.config.ConfigRetriever
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.config.configRetrieverOptionsOf
import io.vertx.kotlin.config.configStoreOptionsOf
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

const val TYPE = "file"
const val FORMAT = "yaml"
const val PATH_KEY = "path"
const val DEFAULT_PATH = "config/config.yaml"

fun loadYamlConfig(vertx: Vertx, configPath: String = DEFAULT_PATH): Future<JsonObject> {

    // load the config from yaml file
    val configFile = configStoreOptionsOf(
        type = TYPE,
        format = FORMAT,
        config = json {
            obj(PATH_KEY to configPath)
        })

    val retriever = ConfigRetriever.create(
        vertx, configRetrieverOptionsOf(
            //include all default config loading, e.g. Env vars
            includeDefaultStores = true,
            stores = listOf(configFile)
        )
    )

    return retriever.config
}