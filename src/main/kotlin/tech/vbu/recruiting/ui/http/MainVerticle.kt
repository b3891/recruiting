package tech.vbu.recruiting.ui.http

import io.vertx.ext.web.Router
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.flywaydb.core.Flyway
import org.postgresql.ds.PGConnectionPoolDataSource
import tech.vbu.recruiting.application.MessageQueryService
import tech.vbu.recruiting.infrastructure.PostgresMessageLookupService
import tech.vbu.recruiting.ui.http.message.getMessagesDataHandlerOf

class MainVerticle : CoroutineVerticle() {

    override suspend fun start() {
        val config = loadYamlConfig(vertx).await().getJsonObject(
            "dataSource"
        )
        val dataSource = PGConnectionPoolDataSource().apply {
            setUrl(config.getString("url"))
            user = config.getString("user")
            password = config.getString("password")
        }

        val messageLookupService = PostgresMessageLookupService(dataSource)
        val messageQueryService = MessageQueryService(messageLookupService)

        val router = Router.router(vertx)

        router.get("/messages")
            .handler(getMessagesDataHandlerOf(messageQueryService::getMessage))


        Flyway.configure().dataSource(config.getString("url"), config.getString("user"), config.getString("password"))
            .load()
            .migrate()

        vertx.createHttpServer()
            .requestHandler(router)
            .listen(9000)
    }
}


