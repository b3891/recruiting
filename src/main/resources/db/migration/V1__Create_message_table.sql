CREATE TABLE message
(
    id           BIGSERIAL PRIMARY KEY,
    data         VARCHAR NOT NULL,
    content_type VARCHAR NOT NULL
);