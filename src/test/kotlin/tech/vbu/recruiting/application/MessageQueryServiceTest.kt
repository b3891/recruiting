package tech.vbu.recruiting.application

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import tech.vbu.recruiting.application.model.MessageQueryResult
import tech.vbu.recruiting.domain.ContentType
import tech.vbu.recruiting.domain.Message
import kotlin.test.assertEquals

class MessageQueryServiceTest {

    @Test
    fun `getMessages returns hello world Message`() {
        val lookupService = TestMessageLookupService()
        val messageQueryService = MessageQueryService(lookupService)

        val future = messageQueryService.getMessage()

        val messages = future.get().entries
        assertThat(future.isDone).isTrue()
        assertThat(messages).hasSize(1)
        assertEquals(messages.first().data, "Hello World")
        assertEquals(messages.first().contentType.type, "text/plain")
    }

    @Test
    fun `getMessages returns empty Collection`() {
        val lookupService = TestMessageLookupService(emptyList())
        val messageQueryService = MessageQueryService(lookupService)

        val future = messageQueryService.getMessage()

        val messages = future.get().entries
        assertThat(future.isDone).isTrue()
        assertThat(messages).hasSize(0)
    }

    @Test
    fun `getMessages throws Exception`() {
        val lookupService = TestMessageLookupService(throws = true)
        val messageQueryService = MessageQueryService(lookupService)

        assertThrows<Exception> {
            messageQueryService.getMessage()
        }
    }
}

class TestMessageLookupService(
    private val collectionOfMessages: Collection<Message> = listOf(
        Message(
            "Hello World",
            ContentType("text/plain")
        )
    ),
    private val throws: Boolean = false
) : MessageLookupService {
    override fun getMessages(): MessageQueryResult {

        return if (!throws) MessageQueryResult(collectionOfMessages)
        else {
            throw Exception()
        }
    }

}