# VBU Recruiting project

This project serves as a basis for discussion with applicants. They should evaluate the project or the changes to this project. And after that, they present their results in the job interview.

## Building

To launch the tests:
```
mvn clean test
```

To package the application:
```
mvn clean package
```

## To run the application standalone

Start it via docker-compose
```
docker-compose up
```

## Call the webservice:
```
curl localhost:9000/messages'
```

## Deployment to Kubernetes

Deployment is done automatically using Gitlab CI.